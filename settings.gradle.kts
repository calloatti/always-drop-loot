pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenCentral()
        jcenter()
        maven(url = "https://maven.fabricmc.net/")
    }
}

rootProject.name = "always-drop-loot"
